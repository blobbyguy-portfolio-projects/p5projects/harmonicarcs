let canvasHeight = 500;
let canvasWidth = 500;

    
let centreX = canvasWidth/2;
let centreY = canvasHeight/2;

let numberOfCircles = 15;

var spacing = canvasWidth/numberOfCircles;


function setup() {
  createCanvas(canvasWidth, canvasHeight);
  frameRate(60);
}

function draw() {

  background(255);
  strokeWeight(3);
  stroke(255);
  line(0, centreY, canvasWidth, centreY);

  //Draw Large Circles
  noFill();
  stroke(200);
  for(var x = 1; x < numberOfCircles; x++){
    let diameter = spacing * x
    //circle(centreX,centreY, diameter);
    arc(centreX, centreY, diameter, diameter, PI, 0);
  }

  //Draw Point Circles
  fill(255,0,0);
  strokeWeight(3)
  stroke(255,0,0);

  //Dividing by 1000 makes the math more natural
  let time = millis() / 1000;
  let velocity = 2;

  for (var x = 1; x < numberOfCircles; x++){
    //Reduce velocity of each circle slightlyl
    let adjustedVelocity = velocity - (x * 0.03);
    let distance = (Math.PI + (time * adjustedVelocity)) % (TWO_PI);
    //Create the oscellation
    let adjustedDistance = distance >= PI ? distance : TWO_PI - distance;

    let diameter = spacing * x;
    let radius = diameter/2;

    let circleX = radius * Math.cos(adjustedDistance) + centreX;
    let circleY = radius * Math.sin(adjustedDistance) + centreY;
    circle(circleX, circleY, 10);
  }
}
